package com.watkinsr.ecgp;

public class Tuple<X, Y> { 
	  public X x; 
	  public final Y y; 
	  public Tuple(X x, Y y) { 
	    this.x = x; 
	    this.y = y; 
	  } 
	} 
